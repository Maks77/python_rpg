class Editor:
    def __init__(self):
        self.propsArr = [{'prop': 'race', 'value': ''},
                         {'prop': 'gender', 'value': ''},
                         {'prop': 'skinColor', 'value': ''},
                         {'prop': 'weight', 'value': ''},
                         {'prop': 'tatoo', 'value': ''},
                         {'prop': 'hairColor', 'value': ''}]

    def startCreate(self):
        for i in self.propsArr:
            i['value'] = input('Enter value of ' + i['prop'])

        self.createHero()


    def createHero(self):
        for item in self.propsArr:
            print(item['value'])


class Hero:
    def __init__(self, race, gender, skinColor, weight, tatoo, hairColor):
        self.race = race
        self.gender = gender
        self.skinColor = skinColor
        self._weight = weight
        self.tatoo = tatoo
        self.hairColor = hairColor


class Human(Hero):
    def __init__(self, *args):
        super().__init__(*args)


class Nord(Human):
    def __init__(self, *args):
        super().__init__(*args)


class Breton(Human):
    def __init__(self, *args):
        super().__init__(*args)


class Dunmer(Human):
    def __init__(self, *args):
        super().__init__(*args)


class Altmer(Human):
    def __init__(self, *args):
        super().__init__(*args)


class Humanoid(Hero):
    def __init__(self, *args, tatoo = None, hairColor = None):
        self.tatoo = tatoo
        self.hairColor = hairColor
        super().__init__(*args, tatoo, hairColor)


class Khadjit(Humanoid):
    def __init__(self, *args):
        super().__init__(*args)


class Argonian(Humanoid):
    def __init__(self, *args):
        super().__init__(*args)


edit = Editor()

raceProp = input('Enter the race of your character: ')
genderProp = input('Choose the gender of your character: ')
skinColorProp = input('Choose the gender of your character: ')


